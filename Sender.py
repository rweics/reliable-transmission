import sys
import getopt
import os

import Checksum
import BasicSender

'''
This is a skeleton sender class. Create a fantastic transport protocol here.
'''
class Sender(BasicSender.BasicSender):
    def __init__(self, dest, port, filename, debug=False, sackMode=False):
        super(Sender, self).__init__(dest, port, filename, debug)
        self.sackMode = sackMode
        if sackMode:
            self.received = set()
        self.data_size = 1472
        self.window_begin = 0
        self.next_seqno = 0
        self.dup_ack = 0
        self.one_packet = False
        self.finished = False

        file_len = os.path.getsize(filename)
        self.num_packets = (file_len / self.data_size if file_len % self.data_size == 0 else file_len / self.data_size + 1)

        if self.num_packets == 1:
            self.num_packets = 2
            self.one_packet = True

    # Main sending loop.
    def start(self):
        while (not self.finished):
            ack = self.receive(0.5)
            if ack: self.handle_ack(ack)
            elif self.window_begin == 0: self.send_next_packet()
            else: self.handle_timeout()

    def send_next_packet(self):
        if self.window_begin == 0: start = self.window_begin
        else: start = self.next_seqno 
        for i in range(start, self.window_begin+5):
            if i < self.num_packets:
                if i == 0:
                    self.send(self.make_packet("start",i,self.get_chunk(i)))
                elif i == self.num_packets-1:
                    self.send(self.make_packet("end",i,self.get_chunk(i)))
                else:
                    self.send(self.make_packet("data",i,self.get_chunk(i)))
                self.next_seqno += 1

    def handle_timeout(self):
        if self.sackMode:
            queue = list()
            for p in range(self.window_begin, self.next_seqno):
                if p not in self.received:
                    queue.append(p)
        else:
            queue = [self.window_begin]
        for i in queue:
            if i == 0:
                self.send(self.make_packet("start",i,self.get_chunk(i)))
            elif i == self.num_packets-1:
                self.send(self.make_packet("end",i,self.get_chunk(i)))
            else:
                self.send(self.make_packet("data",i,self.get_chunk(i)))

    def handle_ack(self, ack):
        if Checksum.validate_checksum(ack):
            if ack.split('|')[0] == 'sack':
                self.handle_sack(ack)
            else:
                ack_no = int(ack.split('|')[1])
                if ack_no > self.window_begin:
                    self.window_begin = ack_no
                    self.dup_ack = 0
                    if ack_no == self.num_packets: self.finished = True
                    else: self.send_next_packet()
                elif self.window_begin == ack_no:
                    self.dup_ack += 1
                    self.handle_dup_ack(ack)
                else:
                    pass

    def handle_dup_ack(self, ack):
        if self.dup_ack == 3:
            self.handle_timeout()

    def handle_sack(self, sack):
        body = sack.split('|')[1]
        seqno = body.split(';')[0]
        received = body.split(';')[1].split(',')
        for r in received:
            if r != '': self.received.add(int(r))
        ack = "%s%s" % ("ack|%s|" % seqno, Checksum.generate_checksum("ack|%s|" % seqno))
        self.handle_ack(ack)

    def get_chunk(self, seqno):
        if (self.one_packet and seqno == 1):
            return ''
        self.infile.seek(seqno * self.data_size)
        return self.infile.read(self.data_size)

    def log(self, msg):
        if self.debug:
            print msg

'''
This will be run if you run this script from the command line. You should not
change any of this; the grader may rely on the behavior here to test your
submission.
'''
if __name__ == "__main__":
    def usage():
        print "BEARS-TP Sender"
        print "-f FILE | --file=FILE The file to transfer; if empty reads from STDIN"
        print "-p PORT | --port=PORT The destination port, defaults to 33122"
        print "-a ADDRESS | --address=ADDRESS The receiver address or hostname, defaults to localhost"
        print "-d | --debug Print debug messages"
        print "-h | --help Print this usage message"
        print "-k | --sack Enable selective acknowledgement mode"

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                               "f:p:a:dk", ["file=", "port=", "address=", "debug=", "sack="])
    except:
        usage()
        exit()

    port = 33122
    dest = "localhost"
    filename = None
    debug = False
    sackMode = False

    for o,a in opts:
        if o in ("-f", "--file="):
            filename = a
        elif o in ("-p", "--port="):
            port = int(a)
        elif o in ("-a", "--address="):
            dest = a
        elif o in ("-d", "--debug="):
            debug = True
        elif o in ("-k", "--sack="):
            sackMode = True

    s = Sender(dest, port, filename, debug, sackMode)
    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        exit()
